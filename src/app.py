
from flask import Flask, jsonify, request
import os
from google.cloud import vision
from collections import OrderedDict 
from utils import ocr,processing,text_extraction
from functools import reduce
from time import time
import numpy as np
from PIL import Image
import json
import cv2
app = Flask(__name__)

UPLOAD_FOLDER = os.path.basename('./files')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['JSON_AS_ASCII'] = False

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r"GoogleVIsion.json"
client = vision.ImageAnnotatorClient()

    
@app.route('/v1/label_extraction', methods=['POST','GET'])
def upload_file():
    if request.method == "GET":
        return jsonify({"TEXTO": "Seja Bem vindo a API de OCR, faça uma requisição POST e envie os parâmetros necessários para sua consulta"})
    elif request.method == "POST":
        try:
            inicio = time()
            file = request.files['image']
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))
            image_ori = processing.resize(processing.rotate(np.asarray(Image.open(file))) ,1500)
            os.remove(UPLOAD_FOLDER+"/"+file.filename)
            try:
                response = ocr.detect_document_text(image_ori)
                if response.ByteSize() == 0 or response == "Unreadable image!" : 
                    os.remove(UPLOAD_FOLDER+"/"+file.filename)
                    return  jsonify({"response":"Nothing to read"})
                poligonos = processing.find_polygons(response)
                
                extraction = text_extraction.line_segmentation(response,poligonos)
                boxes = reduce(lambda a,b : {**a, **b},extraction)
                boxes2 = text_extraction.camp_segmentation(boxes)
            except Exception as e:
                os.remove(UPLOAD_FOLDER+"/"+file.filename)
                return  jsonify({"response": f"error processing image"})
                
            
            output = {}
            for campo,tipo in text_extraction.campos:
                found = text_extraction.find_label(campo,tipo,boxes2)   
                if len(list(found.values())[0]) > 0:
                    output.update(found)

            for key in output: 
                if key != 'FILIAÇÃO':
                    output[key] = output[key][0]
            
            if len(output) < 2:
                output = {}
                for campo,tipo in text_extraction.campos:
                    found = text_extraction.get_label(campo,tipo,boxes2)    
                    if len(list(found.values())[0]) > 0:
                        output.update(found)
                for key in output: 
                    output[key] = output[key][0]
            
            resposta = OrderedDict({"complete_text":response.text,
                                    "labels": output})
                                    
                                    
                                    
            fim = time()
            resposta.update({"runtime": f"{round(fim - inicio,2)}s"})
            return  jsonify(resposta)
        except Exception as e:
        
            return  jsonify({f"response": f"error processing image\n{e}"})
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')