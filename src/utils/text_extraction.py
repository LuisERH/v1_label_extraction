import json
import numpy as np
from functools import reduce
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from google.protobuf.json_format import MessageToJson
import unicodedata
from nltk import everygrams
from dateparser.search import search_dates
from datetime import datetime
from string import digits,punctuation
from validate_docbr import CPF
cpf = CPF()
from jellyfish import jaro_distance
import cv2

def remove_accents(input_str):
    nfkd_form = unicodedata.normalize("NFKD", input_str.upper().replace("\n", " "))
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])


def find_in_box(box, polygons, indice):
    box = Polygon(np.squeeze(box))
    caixas, texto_conf, caixas_texto = [], [], []
    output = []
    for polygon, desc, conf in polygons:
        if box.contains(polygon.centroid):
            four_points = np.array([polygon.exterior.coords[:4]], dtype=np.int32)
            caixas.append(four_points)
            caixas_texto.append([four_points, desc, conf])
    if len(caixas) == 0:
        return ""  # caso o contorno do campo não conter nenhum texto

    for four_points, desc, conf in caixas_texto:
        texto_conf.append((desc, four_points))
    texto_conf = np.array(texto_conf)

    for four_points, desc, conf in caixas_texto:
        output.append(
            {
                "description": str(desc),
                "confidence": float(conf),
                "bounding_box": np.squeeze(four_points).tolist(),
            }
        )

    output = {
        indice: {
            "box_full_text": " ".join(texto_conf[:, 0]),
            "bounding_box_block": box.exterior.coords[:4],
            "words": output,
        }
    }
    return output


    
def find_in_box(box, polygons, indice):
    box = Polygon(np.squeeze(box))
    caixas, texto_conf, caixas_texto = [], [], []
    output = []
    for polygon, desc, conf in polygons:
        if box.contains(polygon.centroid):
            four_points = np.array([polygon.exterior.coords[:4]], dtype=np.int32)
            caixas.append(four_points)
            caixas_texto.append([four_points, desc, conf])
    if len(caixas) == 0:
        return ""  # caso o contorno do campo não conter nenhum texto

    for four_points, desc, conf in caixas_texto:
        texto_conf.append((desc, four_points))
    texto_conf = np.array(texto_conf,dtype='object')

    for four_points, desc, conf in caixas_texto:
        output.append(
            {
                "description": str(desc),
                "confidence": float(conf),
                "bounding_box": np.squeeze(four_points).tolist(),
            }
        )

    output = {
        indice: {
            "box_full_text": " ".join(texto_conf[:, 0]),
            "bounding_box_block": box.exterior.coords[:4],
            "words": output,
        }
    }
    return output

def line_segmentation(response,polygons):
    median = int(np.median([polygon_width(p)[0] for  p in np.array(polygons)[:,0]]))
    simbolos = []
    for page in response.pages:
        for block in page.blocks:
            for paragraph in block.paragraphs:
                palavra = []
                for word in paragraph.words:
                    for symbol in word.symbols:
                        simbolos.append(json.loads(MessageToJson(symbol)))

    breaks ,output  = [], []
    starts = [simbolos[0]]
    for i in range(len(simbolos)):
        try: 
            b = simbolos[i]["property"]["detectedBreak"]['type']
            
            if b == "EOL_SURE_SPACE" or  b =="LINE_BREAK":
                breaks.append(simbolos[i])
                starts.append(simbolos[i+1])
        except: pass

def find_in_box(box, polygons, indice):
    box = Polygon(np.squeeze(box))
    caixas, texto_conf, caixas_texto = [], [], []
    output = []
    for polygon, desc, conf in polygons:
        if box.contains(polygon.centroid):
            four_points = np.array([polygon.exterior.coords[:4]], dtype=np.int32)
            caixas.append(four_points)
            caixas_texto.append([four_points, desc, conf])
    if len(caixas) == 0:
        return ""  # caso o contorno do campo não conter nenhum texto

    for four_points, desc, conf in caixas_texto:
        texto_conf.append((desc, four_points))
    texto_conf = np.array(texto_conf)

    for four_points, desc, conf in caixas_texto:
        output.append(
            {
                "description": str(desc),
                "confidence": float(conf),
                "bounding_box": np.squeeze(four_points).tolist(),
            }
        )

    output = {
        indice: {
            "box_full_text": " ".join(texto_conf[:, 0]),
            "bounding_box_block": box.exterior.coords[:4],
            "words": output,
        }
    }
    return output

    for indice, (s, b) in enumerate(zip(starts,breaks),1):
        s =  s['boundingBox']['vertices']
        b =  b['boundingBox']['vertices']
        if np.diff((s[0]['x'],b[0]['x'])) > 0 and np.diff((s[0]['x'],b[0]['x']))< median*0.3:
            linhas = find_in_box(np.array([[b[1]['x'],b[1]['y']],
                                          [b[2]['x'],b[2]['y']],
                                          [s[3]['x'],s[3]['y']],
                                          [s[0]['x'],s[0]['y']]],dtype=np.int32),
                                           polygons,
                                           indice)
        
        else:
            linhas = find_in_box(np.array([[s[0]['x'],s[0]['y']],
                                        [b[1]['x'],b[1]['y']],
                                        [b[2]['x'],b[2]['y']],
                                        [s[3]['x'],s[3]['y']]],dtype=np.int32),
                                        polygons,
                                        indice)
                                        
        if linhas == None or linhas == [] :pass
        else:output.append(linhas)
    return output



def sort_words(words):
    bound = words['bounding_box_block']
    max_y, min_y, min_x = int(max([i[1] for i in bound])),int(min([i[1] for i in bound])),int(min([i[0] for i in bound]))
    ponto = Point(min_x,(np.mean([min_y,max_y],dtype=np.float16)))
    sorted_words = []
    for word in words['words']:
        polygon = Polygon(word['bounding_box'])
        sorted_words.append(( ponto.distance(polygon),
                              word['description'],
                              word['confidence'],
                              polygon ))
    return [(round(s[0],2),s[1],s[2],s[3]) for s in sorted(sorted_words, key= lambda w: w[0])]
    
def stop_calc2(sorted_words):
    "compara o espaço entre cada palavra (objetivo disto é saber quando termina a informação que desejamos extrair"
    distance = []
    [distance.append(sorted_words[i][3].distance(sorted_words[i-1][3])) for i in range(2,len(sorted_words))]
    return [i+1 for i,j in enumerate(distance) if j > 54]
    
def clean_word(word, label,mat):
    remove_digits = str.maketrans('', '', digits)
    remove_puncts = str.maketrans('', '', punctuation)
    if match(word,'DIGITALIZADO')[1] > 0.8: return '' 
    if match(word,'PESQUISADO')[1] > 0.8: return '' 
    word = word.translate(remove_digits).translate(remove_puncts).replace(mat[0],'').strip()
    return word

def get_name(boxes, label):
    candidates = []
    for i,box in enumerate(boxes,1):
        bound = box[i]['bounding_box_block']
        max_y, min_y, min_x = int(max([i[1] for i in bound])),int(min([i[1] for i in bound])),int(min([i[0] for i in bound]))
        ponto = Point(min_x,(np.mean([min_y,max_y],dtype=np.float16)))
        sorted_words = []
        for word in box[i]['words']:
            polygon = Polygon(word['bounding_box'])
            sorted_words.append((ponto.distance(polygon),
                                 word['description'],
                                 word['confidence'],
                                 polygon))
            
        sorted_words = [(round(s[0],2),remove_accents(s[1]),s[2],s[3]) for s in sorted(sorted_words, key= lambda w: w[0])]
        quebra= stop_calc2(sorted_words)
        sorted_words = sorted_words if len(quebra)== 0 else sorted_words[:quebra[0]+1]
        mat = match(sorted_words[0][1],label) 
        if mat[1] > 0.77:
            del sorted_words[0]
            candidates.append(np.array([[clean_word(desc,label,mat),conf]
                                        for _,desc,conf,_ in sorted_words
                                        if clean_word(desc,label,mat) != '']))
    result = [(' '.join(c[:,0]), np.mean(c[:,1].astype(np.float16)))for c in candidates if c.size > 0 and len(c[:,0])>1 and len(c[:,0]) < 8]
    return max(result) if len(result)>0 else result


def stop_calc(sorted_words,segments):
    "compara o espaço entre cada palavra (objetivo disto é saber quando termina a informação que desejamos extrair"
    distance =[]
    [distance.append(sorted_words[i][3].distance(sorted_words[i-1][3])) for i in range(1,len(sorted_words))]
    output = [i+1 for i,j in enumerate(distance) if j > 54]
    output.append(len(sorted_words))
    mark = 0
    for quebra in output:
        segments.append(sorted_words[mark:quebra])
        mark = quebra
    return segments

def line_segmentation(response,polygons):
    median = int(np.median([polygon_width(p)[0] for  p in np.array(polygons)[:,0]]))
    simbolos = []
    for page in response.pages:
        for block in page.blocks:
            for paragraph in block.paragraphs:
                palavra = []
                for word in paragraph.words:
                    for symbol in word.symbols:
                        simbolos.append(json.loads(MessageToJson(symbol)))

    breaks ,output  = [], []
    starts = [simbolos[0]]
    for i in range(len(simbolos)):
        try: 
            b = simbolos[i]["property"]["detectedBreak"]['type']
            if b == "EOL_SURE_SPACE" or  b =="LINE_BREAK":
                breaks.append(simbolos[i])
                starts.append(simbolos[i+1])
        except: pass

    for indice, (s, b) in enumerate(zip(starts,breaks),1):
        s =  s['boundingBox']['vertices']
        b =  b['boundingBox']['vertices']
        
        if np.diff((s[0]['x'],b[0]['x'])) > 0 and np.diff((s[0]['x'],b[0]['x']))< median*0.3:
            linhas = find_in_box(np.array([[b[1]['x'],b[1]['y']],
                                          [b[2]['x'],b[2]['y']],
                                          [s[3]['x'],s[3]['y']],
                                          [s[0]['x'],s[0]['y']]],dtype=np.int32),
                                           polygons,
                                           indice)
        
        else:
            linhas = find_in_box(np.array([[s[0]['x'],s[0]['y']],
                                        [b[1]['x'],b[1]['y']],
                                        [b[2]['x'],b[2]['y']],
                                        [s[3]['x'],s[3]['y']]],dtype=np.int32),
                                        polygons,
                                        indice)
        if len(linhas) == 0:pass
        else:output.append(linhas)
    return output

def camp_segmentation(boxes):
    boxes_test = boxes.copy()
    n = 0
    for i in boxes:
        segments = []
        sorted_words = sort_words(boxes[i])
        segments= stop_calc(sorted_words,segments)

        for segment in segments:
            output = []
            n+=1
            y_max = np.array(segment[0][3].exterior.coords[:4],dtype=np.int32)[:,1].max()
            y_min = np.array(segment[0][3].exterior.coords[:4],dtype=np.int32)[:,1].min()
            x_min = np.array(segment[0][3].exterior.coords[:4],dtype=np.int32)[:,0].min()

            y_max_ = np.array(segment[-1][3].exterior.coords[:4],dtype=np.int32)[:,1].max()
            y_min_ = np.array(segment[-1][3].exterior.coords[:4],dtype=np.int32)[:,1].min()
            x_max_ = np.array(segment[-1][3].exterior.coords[:4],dtype=np.int32)[:,0].max()

            boxes_test[n] = {'box_full_text' : " ".join([i[1] for i in segment])}
            boxes_test[n]['bounding_box_block'] = [(x_min, y_min), (x_max_,y_min_ ), (x_max_, y_max_), (x_min, y_max)]
            for seg in segment:
                output.append({"description": seg[1],
                            "confidence": seg[2],
                            "bounding_box":seg[3].exterior.coords[:4]})
            boxes_test[n]['words'] = output
        
    return boxes_test
    
def match(texto_ocr, alvo):
    texto_ocr = remove_accents(texto_ocr)
    alvo = remove_accents(alvo)
    ngrams = list(everygrams(texto_ocr.split(),1 ,len(alvo.split())+2))
    texts = [" ".join(i) for i in ngrams]

    sentencas = [sentenca for sentenca in texts]
    percents = [jaro_distance(alvo,sentenca) for sentenca in texts]
    return max(zip(sentencas,percents), key=lambda x: x[1])

def find_and_parse(text):
    date = search_dates(text)
    return date if date == None else datetime.strftime(date[0][1], "%d/%m/%Y")
    

def polygon_width(poly):
    if str(type(poly)) == "<class 'shapely.geometry.polygon.Polygon'>":
        box= poly.minimum_rotated_rectangle
        x, y = box.exterior.coords.xy
        edge_length = (Point(x[0], y[0]).distance(Point(x[1], y[1])), Point(x[1], y[1]).distance(Point(x[2], y[2])))
        length = max(edge_length)
        width = min(edge_length)
        return width, length

def filter_nome(candidates):
    for i,c in enumerate(candidates):
        c = c.upper()
        if not any(i.isdigit() for i in c) and len(c.split())>2:
            if c.split()[0] == "DR" or c.split()[0] == "MR": " ".join(c.split()[1:])
            if c.find(".") != -1:candidates[i] = c[c.find(".")+1:].strip()
            if c.find(",") != -1:candidates[i] = c[c.find(",")+1:].strip()
        else:
            del candidates[i]
    if len(set(candidates)) == 2: return [" ".join(candidates)]
    return  list(set(candidates))

def filter_filiacao(candidates):
    for i,c in enumerate(candidates):
        if not any(i.isdigit() for i in c) and len(c.split())>2:
            if c.find(".") != -1:
                candidates[i] = c[c.find(".")+1:].strip()
        else:
            del candidates[i]
    if len(candidates) >1: return candidates[:2]
    return  list(set(candidates))

def filter_numero(candidates):
     return  list(set(filter(lambda x: sum(c.isdigit() for c in x) > 1, map(clean_punct,candidates))))

def filter_data(candidates):
    return  list(set(filter(lambda x: x != None, map(find_and_parse,candidates))))

def filter_cpf(candidates):
    return  list(set(map(cpf.mask,filter(lambda x:cpf.validate(x) , filter_numero(candidates)))))

def filter_sangue(candidates):
    casos = ['A', 'B', 'AB', 'O', 'A+', ' B+', 'O+', 'AB+', 'A-', 'B-', 'O- ', 'AB-']
    for i,c in enumerate(candidates):
        if c not in casos:
            del candidates[i]
    return  list(set(candidates))

def filter_fator(candidates):
    casos = ['+','-','POSITIVO','NEGATIVO','POS','NEG']
    for i,c in enumerate(candidates):
        if c not in casos:
            del candidates[i]
    return list(set(candidates))

def clean_punct(text):
    remove_puncts = str.maketrans('', '', '''!"#$%&'()*+,.:;<=>?@[\]^_`{|}~''')
    text = text.translate(remove_puncts)
    return text
def find_label(labels, tipo,boxes2):
    candidates=[]
    teste = []
    for label in labels:
        for box in boxes2:
            if match(boxes2[box]['box_full_text'],label)[1] > 0.9:
                found_box = Polygon(boxes2[box]['bounding_box_block'])
                for index in boxes2.copy():
                    distance = found_box.distance(Polygon(boxes2[index]['bounding_box_block']))
                    y_alvo = np.array(boxes2[box]['bounding_box_block'])[:,1].max()
                    y_candidato = np.array(boxes2[index]['bounding_box_block'])[:,1].min()
                    teste.append([distance, y_alvo,y_candidato, boxes2[index]['box_full_text']])
                for distance, y_alvo,y_candidato,texto in sorted(teste)[:3]:
                    if y_alvo < y_candidato and texto != boxes2[box]['box_full_text']: 
                        st = texto.replace(label,"")
                        for i in sum(campos[:,0], []):
                            if st.find(i) != -1: st = ''
                        if st != '' :candidates.append(remove_accents(st).strip())
            
    if tipo == "NUMERO": return {labels[0] : filter_numero(candidates)}
    elif tipo == "DATA": return {labels[0] : filter_data(candidates)}
    elif tipo == "FILIACAO": return {labels[0] : filter_filiacao(candidates)}
    elif tipo == "CPF":  return {labels[0] : filter_cpf(candidates)}
    elif tipo == "NOME": return {labels[0] : filter_nome(candidates)}
    elif tipo == "SANGUE": return {labels[0] : filter_sangue(candidates)}
    elif tipo == "FATOR": return {labels[0] : filter_fator(candidates)}
    else: return {labels[0] :list(set(candidates))}

def get_label(labels,tipo,boxes2):
    candidates = []
    for label in labels:
        for i in boxes2:
            bound = boxes2[i]['bounding_box_block']
            max_y, min_y, min_x = int(max([i[1] for i in bound])),int(min([i[1] for i in bound])),int(min([i[0] for i in bound]))
            ponto = Point(min_x,(np.mean([min_y,max_y],dtype=np.float16)))
            sorted_words = []
            for word in boxes2[i]['words']:
                polygon = Polygon(word['bounding_box'])
                sorted_words.append((ponto.distance(polygon),
                                    word['description'],
                                    word['confidence'],
                                    polygon))
                
            sorted_words = np.array([(round(s[0],2),remove_accents(s[1]),s[2],s[3]) for s in sorted(sorted_words, key= lambda x: x[0])])
            mat = match(' '.join(sorted_words[:,1]).upper(),label)
            if mat[1] > 0.9:
                frase = ' '.join(sorted_words[:,1]).partition(mat[0])[2]
                for camp in sum(campos[:,0], []):
                    frase = frase.partition(camp)[0]  if frase.find(camp) != -1 else frase
                candidates.append(frase.strip())
    if tipo == "NUMERO": return {labels[0] : filter_numero(candidates)}
    elif tipo == "DATA": return {labels[0] : filter_data(candidates)}
    elif tipo == "FILIACAO": return {labels[0] : filter_filiacao(candidates)}
    elif tipo == "CPF":  return {labels[0] : filter_cpf(candidates)}
    elif tipo == "NOME": return {labels[0] : filter_nome(candidates)}
    elif tipo == "SANGUE": return {labels[0] : filter_sangue(candidates)}
    elif tipo == "FATOR": return {labels[0] : filter_fator(candidates)}
    return {labels[0] :list(set(candidates))}

def remove_accents(input_str):
    nfkd_form = unicodedata.normalize("NFKD", input_str.upper().replace("\n", " "))
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])

campos = np.array([[["NOME"],"NOME"],
          [["FILIAÇÃO"],"FILIACAO"],
          [["CATEGORIA PROFISSIONAL", "PROFISSÃO"],""],
          [["DATA DE NASCIMENTO"],"DATA"],
          [["DATA DE CONCLUSÃO"],"DATA"],
          [["DATA DE EXPEDIÇÃO","EXPEDIÇÃO"],"DATA"],
          [["DATA DA DIPLOMAÇÃO"],"DATA"],
          [["CRF"],"NUMERO"],
          [["DIPLOMADO PELA", "DIPLOMA"],""],
          [["NATURALIDADE"],""],
          [["UNIDADE DE SAÚDE"],""],
          [["NACIONALIDADE"],"UF"],
          [["EMISSÃO"],"DATA"],
          [["GRUPO SANGUÍNEO"],"SANGUE"],
          [["FATOR RH"],"FATOR"],
          [["RG"],"NUMERO"],
          [["TÍTULO DE ELEITOR","TÍTULO"],"NUMERO"],
          [["ZONA"],"NUMERO"],
          [["SEÇÃO"],"NUMERO"],
          [["LOCAL"],""],
          [["CPF"],"CPF"],
          [["NUMERO"],"NUMERO"],
          [["SERIE"],"NUMERO"],
          [["AGENCIA","AG"],"NUMERO"],
          [["CONTA CORRENTE","CORRENTE","CONTA","C/C","C / C"],"NUMERO"]],dtype='object')
