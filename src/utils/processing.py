from utils import text_extraction
import cv2
import numpy as np
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from scipy.ndimage import interpolation as inter
from PIL import Image
import math
from deskew import determine_skew



def find_polygons(response):
    polygons = [] # Polígonos que envolvem as palavras (oriundos da API do Google Vision)
    for page in response.pages:
        for block in page.blocks:
            for paragraph in block.paragraphs:
                for word in paragraph.words:
                    letra = []
                    confidence = []
                    for symbol in word.symbols:
                        letra.append(symbol.text)
                        confidence.append(symbol.confidence) 
                    description = "".join(letra)
                    points = word.bounding_box.vertices
                    polygon = Polygon([[points[0].x, points[0].y],
                                       [points[1].x, points[1].y],
                                       [points[2].x, points[2].y],
                                       [points[3].x, points[3].y]])
                    polygons.append((polygon,description,np.mean(confidence,dtype=np.float16)))
    return polygons

def noise_removal(image):
    """Remove ruídos, tranforma a imagem em escala de cinza e devolve apenas a estrutura encontrada"""
    try:
        rgb_planes = cv2.split(image)
        result_norm_planes = []
        for plane in rgb_planes:
            dilated_img = cv2.dilate(plane, np.ones((2,2), np.uint8))
            bg_img = cv2.medianBlur(dilated_img, 21)
            diff_img = 255 - cv2.absdiff(plane, bg_img)
            norm_img = cv2.normalize(diff_img,None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
            result_norm_planes.append(norm_img)
        image = cv2.merge(result_norm_planes)
    except Exception as e:
        print(e)
        return f"Error: {e}"
    return image

def clarify_text(image): 
    """Aumenta o contraste das palavras"""
    image = cv2.adaptiveThreshold(image,
                                maxValue=255,
                                adaptiveMethod=cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                thresholdType=cv2.THRESH_BINARY,
                                blockSize=77,
                                C=3)
    return image

def rotate_img(image):
    """rotaciona a imagem caso estiver torta"""
    image =  clarify_text(image)
    coords = np.column_stack(np.where(image > 0))
    angle = cv2.minAreaRect(coords)[-1]
    angle = -(90 + angle) if angle < -45 else -angle
    (h, w) = image.shape[:2]
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, angle, 1.0)
    return cv2.warpAffine(image, M, (w, h),
                          flags=cv2.INTER_CUBIC,
                          borderMode=cv2.BORDER_REPLICATE)


    
def preprocess_image(image):
    """Preprocessamento da imagem completo"""
    image = rotate_img(image)
    return image

def find_polygons(response):
    polygons = []  # Polígonos que envolvem as palavras (oriundos da API do Google Vision)
    for page in response.pages:
        for block in page.blocks:
            for paragraph in block.paragraphs:
                for word in paragraph.words:
                    letra = []
                    confidence = []
                    for symbol in word.symbols:
                        letra.append(symbol.text)
                        confidence.append(symbol.confidence)
                    description = "".join(letra)
                    points = word.bounding_box.vertices
                    polygon = Polygon(
                        [
                            [points[0].x, points[0].y],
                            [points[1].x, points[1].y],
                            [points[2].x, points[2].y],
                            [points[3].x, points[3].y],
                        ]
                    )
                    polygons.append(
                        (polygon, description, np.mean(confidence, dtype=np.float16))
                    )
    return polygons

def process_image(img, alpha=0.5):
    """Remove ruídos, tranforma a imagem em escala de cinza e devolve apenas a estrutura encontrada"""
    try:
        alpha = 0.5
        beta = 1.0 - alpha
        thresh = cv2.threshold(img, 180, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        bitwise = cv2.bitwise_not(thresh)
        erosion = cv2.erode(bitwise, np.ones((1, 1), np.uint8), iterations=4)
        img_bin = cv2.dilate(erosion, np.ones((1, 1), np.uint8), iterations=4)
        kernel_length = np.array(img_bin).shape[1] // 120
        verticle_kernel = cv2.getStructuringElement(
            cv2.MORPH_DILATE, (1, kernel_length)
        )
        hori_kernel = cv2.getStructuringElement(cv2.MORPH_DILATE, (kernel_length, 1))
        kernel = cv2.getStructuringElement(cv2.MORPH_DILATE, (2, 2))
        img_temp1 = cv2.erode(img_bin, verticle_kernel, iterations=4)
        verticle_lines_img = cv2.dilate(img_temp1, verticle_kernel, iterations=4)
        img_temp2 = cv2.erode(img_bin, hori_kernel, iterations=4)
        horizontal_lines_img = cv2.dilate(img_temp2, hori_kernel, iterations=4)
        img_final_bin = cv2.addWeighted(
            verticle_lines_img, alpha, horizontal_lines_img, beta, 0.0
        )
        img_final_bin_erode = cv2.erode(~img_final_bin, kernel, iterations=2)
        img_final_bin = cv2.threshold(
            img_final_bin_erode, 180, 255, cv2.THRESH_BINARY | cv2.THRESH_BINARY
        )[1]
    except Exception as e:
        print(e)
        return "error processing image"
    return img_final_bin

def FLD(image):
    fld = cv2.ximgproc.createFastLineDetector()
    lines = fld.detect(image)
    line_on_image = fld.drawSegments(image, lines)
    gray = cv2.cvtColor(np.array(line_on_image, dtype=np.uint8), cv2.COLOR_BGR2GRAY)
    return cv2.bitwise_not(cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY)[1])


def complete_contours(image):
    """Completa os campos que são formados a partir do processamento de imag"""
    dst = cv2.cornerHarris(image, 2, 7, 0.04)
    thresh = np.float16(0.4 * dst.max())
    for y in range(dst.shape[0]):
        for x in range(dst.shape[1]):
            if dst[y, x] > thresh:
                cv2.rectangle(image, (x - 1, y - 28), (x - 1, y + 28), (0, 0, 255), 1)
                cv2.rectangle(image, (x - 120, y - 1), (x + 120, y - 1), (0, 0, 255), 1)
    image = ~FLD(image)
    return image

def get_contour_precedence(contour, cols):
    tolerance_factor = 10
    origin = cv2.boundingRect(contour)
    return ((origin[1] // tolerance_factor) * tolerance_factor) * cols + origin[0]


def sort_contours(contours,image):
    contours.sort(key=lambda x: get_contour_precedence(x, image.shape[1]))
    return contours

    
def filter_contours(contours, poligonos, image,min_percent = 0.0006, max_percent= 0.095):
    img_width, img_height = image.shape[1], image.shape[0]
    indice = 1
    valid_contours, textos_caixa = [], []
    contours = list(
        filter(
            lambda c: cv2.contourArea(c) > img_width * img_height * min_percent
            and cv2.contourArea(c) < img_width * img_height * max_percent,
            contours,
        )
    )

    contours = sort_contours(contours,image)

    for contour in contours:
        rect = cv2.minAreaRect(contour)
        four_coords = cv2.boxPoints(rect)
        output = text_extraction.find_in_box(np.array([four_coords], dtype=np.int32), poligonos, indice)
        if output != "":
            indice += 1
            valid_contours.append(contour)
            textos_caixa.append(output)

    return valid_contours, textos_caixa


def rotate(image, background =  (0, 0, 0)) -> np.ndarray:
    grayscale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    angle = determine_skew(grayscale)
    old_width, old_height = image.shape[:2]
    angle_radian = math.radians(angle)
    width = abs(np.sin(angle_radian) * old_height) + abs(np.cos(angle_radian) * old_width)
    height = abs(np.sin(angle_radian) * old_width) + abs(np.cos(angle_radian) * old_height)

    image_center = tuple(np.array(image.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    rot_mat[1, 2] += (width - old_width) / 2
    rot_mat[0, 2] += (height - old_height) / 2
    return cv2.warpAffine(image, rot_mat, (int(round(height)), int(round(width))), borderValue=background)

def resize(image, size):
    h, w = image.shape[:2]
    resi = np.divide(size, w, dtype=np.float16)
    resized = cv2.resize(
        image, None, fx=resi, fy=resi, interpolation=cv2.INTER_LINEAR_EXACT
    )
    return resized