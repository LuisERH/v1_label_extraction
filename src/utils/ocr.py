from google.cloud.vision import types,ImageAnnotatorClient
import cv2

def detect_document_text(image, textfile=0, jsonfile=0):
    """Returns document text given an image."""
    
    client = ImageAnnotatorClient()
    image = types.Image(content=cv2.imencode(".jpg", image)[1].tostring())
    image_context = types.ImageContext(language_hints=["pt"])

    response = client.document_text_detection(
        image=image, timeout=1000, image_context=image_context
    )
    document = response.full_text_annotation

    if textfile != 0:
        with io.open(textfile, "wb") as f:
            f.write(document.text.encode("utf-8"))

    if jsonfile != 0:
        with io.open(jsonfile, "wb") as f:
            f.write(str(document))
    return document