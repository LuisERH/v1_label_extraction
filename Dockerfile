FROM python:3.8

WORKDIR /usr/app

COPY requirements.txt . 

RUN apt-get update
RUN python -m pip install --upgrade pip
RUN apt-get install -y libgl1-mesa-dev
RUN pip install -r requirements.txt

COPY . .

ENTRYPOINT ["python"]

WORKDIR /usr/app/src

COPY ./src/credentials/GoogleVIsion.json .

CMD ["app.py"]